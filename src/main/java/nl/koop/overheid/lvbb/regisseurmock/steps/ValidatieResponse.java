package nl.koop.overheid.lvbb.regisseurmock.steps;

public class ValidatieResponse {
    private String status;
    private String statusId;
    private String reportUri;

    public ValidatieResponse(String status) {
        this(status, null);
    }

    public ValidatieResponse(String status, String statusId) {
        this.status = status;
        this.statusId = statusId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getReportUri() {
        return reportUri;
    }

    public void setReportUri(String reportUri) {
        this.reportUri = reportUri;
    }

    public String toString() {
        return String.format("Status = %s%nStatusId = %s%nRapport URI = %s", status, statusId, reportUri);
    }
}
