package nl.koop.overheid.lvbb.regisseurmock;

import java.nio.file.Path;

import com.marklogic.client.DatabaseClient;
import nl.koop.overheid.lvbb.regisseurmock.marklogic.MarkLogicConnection;
import nl.koop.overheid.lvbb.regisseurmock.marklogic.MarkLogicProperties;

public class RegisseurContext {
  private MarkLogicProperties mlProps;
  private Path zipPayload;
  private String oin;
  private String idLevering;
  private DatabaseClient databaseClient;

  public RegisseurContext(MarkLogicProperties mlProps) {
    this.mlProps = mlProps;
    databaseClient = new MarkLogicConnection().getDatabaseClient(this.mlProps);
  }

  public MarkLogicProperties getMlProps() {
    return mlProps;
  }
  public void setMlProps(MarkLogicProperties mlProps) {
    this.mlProps = mlProps;
  }
  public Path getZipPayload() {
    return zipPayload;
  }
  public void setZipPayload(Path zipPayload) {
    this.zipPayload = zipPayload;
  }
  public String getOin() {
    return oin;
  }
  public void setOin(String oin) {
    this.oin = oin;
  }
  public String getIdLevering() {
    return idLevering;
  }
  public void setIdLevering(String idLevering) {
    this.idLevering = idLevering;
  }

  public DatabaseClient getDatabaseClient() {
    return databaseClient;
  }
}
