package nl.koop.overheid.lvbb.regisseurmock.steps;

import com.fasterxml.jackson.databind.JsonNode;
import com.marklogic.client.FailedRequestException;
import com.marklogic.client.ForbiddenUserException;
import com.marklogic.client.ResourceNotFoundException;
import com.marklogic.client.document.JSONDocumentManager;
import com.marklogic.client.document.XMLDocumentManager;
import com.marklogic.client.extensions.ResourceManager;
import com.marklogic.client.io.DOMHandle;
import com.marklogic.client.io.JacksonHandle;
import com.marklogic.client.io.StringHandle;
import com.marklogic.client.util.EditableNamespaceContext;
import com.marklogic.client.util.RequestParameters;
import nl.koop.overheid.lvbb.regisseurmock.RegisseurContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import javax.xml.xpath.XPathExpressionException;

public class ValideerAanleveringStep extends ResourceManager {
    private static final Logger LOG = LoggerFactory.getLogger(ValideerAanleveringStep.class);
    static final public String NAME = "projects-opera-opera-validatie";
    private RegisseurContext context;
    private XMLDocumentManager xmlDocMgr;
    private JSONDocumentManager jsonDocMgr;
    private EditableNamespaceContext namespaces;

    public ValideerAanleveringStep(RegisseurContext context) {
        this(context.getMlProps().getWriterUser(), context.getMlProps().getWriterPassword(), context);
    }
    public ValideerAanleveringStep(String user, String password, RegisseurContext  context) {
        super();
        this.context = context;
        xmlDocMgr = this.context.getDatabaseClient().newXMLDocumentManager();
        jsonDocMgr = this.context.getDatabaseClient().newJSONDocumentManager();
        this.context.getDatabaseClient().init(NAME, this);
 // configure namespace bindings for the XPath processor
        this.namespaces = new EditableNamespaceContext();
        this.namespaces.setNamespaceURI("prc", "urn:koop:lvbb:process");
    }

    public ValidatieResponse maakValidatieRapport(String oin, String idlevering, boolean overwrite)
            throws ResourceNotFoundException, ForbiddenUserException, FailedRequestException, XPathExpressionException {
        RequestParameters params = new RequestParameters();
        params.add("action", "valideer-aanlevering");
        params.add("oin", oin);
        params.add("idlevering", idlevering);
        params.add("overwrite", String.valueOf(overwrite));

        DOMHandle readHandle = new DOMHandle();
        getServices().put(params, new StringHandle(""), readHandle);
        String status= readHandle.evaluateXPath("//status", String.class);
        String statusId= readHandle.evaluateXPath("//status-id", String.class);
        ValidatieResponse validatieResponse = new ValidatieResponse(status, statusId);
        return validatieResponse;
    }

    public ValidatieResponse pollStatus(String statusId) throws XPathExpressionException {
        //Build up the set of parameters for the service call
        RequestParameters params = new RequestParameters();
        // Add the DoorleverZip service parameter
        params.add("action", "status");
        params.add("status-id", statusId);

        // call the service implementation on the REST Server,
        // returning a ResourceServices object
        DOMHandle readHandle = new DOMHandle();
        readHandle.getXPathProcessor().setNamespaceContext(namespaces);
        getServices().get(params, readHandle);
        String status = readHandle.evaluateXPath("//prc:uitkomst", String.class);
        ValidatieResponse validatieResponse = new ValidatieResponse(status, statusId);
        if (status.equals("succes")) {
            String resultaatUri = readHandle.evaluateXPath("//prc:resultaat-uri", String.class);
            validatieResponse.setReportUri(resultaatUri);
        }
        // do something with result
        return validatieResponse;
    }

    public JsonNode getRapport(String resultaatUri) {
        JacksonHandle handleJSON = new JacksonHandle();
        jsonDocMgr.read(resultaatUri, handleJSON);
        JsonNode node = handleJSON.get();
        return node;
    }
}
