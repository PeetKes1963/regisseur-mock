package nl.koop.overheid.lvbb.regisseurmock.marklogic;

public class MarkLogicProperties {
  private String writerUser;
  private String writerPassword;
  private String adminUser;
  private String adminPassword;
  private String host;
  private int port;
  private String authenticationType;

  public String getWriterUser() {
    return writerUser;
  }
  public void setWriterUser(String writerUser) {
    this.writerUser = writerUser;
  }
  public String getWriterPassword() {
    return writerPassword;
  }
  public void setWriterPassword(String writerPassword) {
    this.writerPassword = writerPassword;
  }
  public String getAdminUser() {
    return adminUser;
  }
  public void setAdminUser(String adminUser) {
    this.adminUser = adminUser;
  }
  public String getAdminPassword() {
    return adminPassword;
  }
  public void setAdminPassword(String adminPassword) {
    this.adminPassword = adminPassword;
  }
  public String getHost() {
    return host;
  }
  public void setHost(String host) {
    this.host = host;
  }
  public int getPort() {
    return port;
  }
  public void setPort(int port) {
    this.port = port;
  }
  public String getAuthenticationType() {
    return authenticationType;
  }
  public void setAuthenticationType(String authenticationType) {
    this.authenticationType = authenticationType;
  }  
}
