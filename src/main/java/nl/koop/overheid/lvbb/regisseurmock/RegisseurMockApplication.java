package nl.koop.overheid.lvbb.regisseurmock;

import com.fasterxml.jackson.databind.JsonNode;
import nl.koop.overheid.lvbb.regisseurmock.steps.MoveToCdsStep;
import nl.koop.overheid.lvbb.regisseurmock.steps.ValidatieResponse;
import nl.koop.overheid.lvbb.regisseurmock.steps.ValideerAanleveringStep;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.nio.file.Paths;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import org.w3c.dom.Document;

import javax.xml.xpath.XPathExpressionException;

@SpringBootApplication
public class RegisseurMockApplication implements CommandLineRunner {
	private static Logger LOG = LoggerFactory.getLogger(RegisseurMockApplication.class);

	@Autowired
	private RegisseurConfig regisseurConfig;

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(RegisseurMockApplication.class);
		app.run(args);
	}

	@Override
	public void run(String... args) throws XPathExpressionException {
		LOG.info("Start RegisseurMock");
		Options options = new Options();

		Option optInput = new Option("z", "zip", true, "zip payload locatie");
		optInput.setRequired(true);
		options.addOption(optInput);
		Option optOin = new Option("o", "oin", true, "identificatie Aanleveraar");
		optOin.setRequired(true);
		options.addOption(optOin);
		Option optIdLevering = new Option("i", "idlevering", true, "identificatie Levering");
		optIdLevering.setRequired(true);
		options.addOption(optIdLevering);

		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd = null;//not a good practice, it serves it purpose 

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			LOG.info(e.getMessage());
			formatter.printHelp("MoveToCDS", options);
			System.exit(0);
		}

		String zipPayloadPath = cmd.getOptionValue("zip");
		String oin = cmd.getOptionValue("oin");
		String idLevering = cmd.getOptionValue("idlevering");
		RegisseurContext context = new RegisseurContext(regisseurConfig.getMarklogic());
		context.setZipPayload(Paths.get(zipPayloadPath));
		context.setOin(oin);
		context.setIdLevering(idLevering);

		LOG.debug("zipPayloadPath=" + zipPayloadPath);
		LOG.debug("oin=" + oin);
		LOG.debug("idlevering=" + idLevering);

		LOG.info(String.format("Run moveToCds with oin %s, idlevering %s and zip %s", oin, idLevering, zipPayloadPath));
		MoveToCdsStep moveToCds = new MoveToCdsStep(context);
		boolean result = moveToCds.process("validatie");
		if (result) {
			ValideerAanleveringStep valideer = new ValideerAanleveringStep(context);
			ValidatieResponse validatieResponse = valideer.maakValidatieRapport(oin, idLevering, false);
			LOG.debug(validatieResponse.toString());
			ValidatieResponse statusResponse = valideer.pollStatus(validatieResponse.getStatusId());
			LOG.debug(statusResponse.toString());
			if (statusResponse.getReportUri() != null) {
				JsonNode rapport = valideer.getRapport(statusResponse.getReportUri());
				if (rapport.get("status").asText().equals("succes"))
					LOG.debug("Validatie succesvol afgerond");
				else
					LOG.debug(rapport.toPrettyString());

			}
		}
		LOG.info("Stop RegisseurMock");
	}
}
