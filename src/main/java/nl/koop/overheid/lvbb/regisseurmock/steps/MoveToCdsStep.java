package nl.koop.overheid.lvbb.regisseurmock.steps;

import com.marklogic.client.datamovement.DataMovementManager;
import com.marklogic.client.datamovement.JobReport;
import com.marklogic.client.datamovement.JobTicket;
import com.marklogic.client.datamovement.WriteBatcher;
import com.marklogic.client.document.ServerTransform;
import com.marklogic.client.io.DocumentMetadataHandle;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Enumeration;
import java.util.UUID;

import nl.koop.overheid.lvbb.regisseurmock.RegisseurContext;
import nl.koop.overheid.lvbb.regisseurmock.marklogic.MarkLogicConnection;

public class MoveToCdsStep {
  private static final Logger LOG = LoggerFactory.getLogger(MoveToCdsStep.class);
  private static DataMovementManager manager;
  private static final String OPERA_COLLECTION = "/opera/options/%s";
  private static final String OPERA_OPTIONS_OPDRACHT_BESTANDEN = "/opera/options/opdrachtbestanden";
  
  private RegisseurContext context;
  
  public MoveToCdsStep(RegisseurContext context) {
    this(context.getMlProps().getWriterUser(), context.getMlProps().getWriterPassword(), context);
  }

  public MoveToCdsStep(String user, String password, RegisseurContext context) {
    this.context = context;
    manager = context.getDatabaseClient().newDataMovementManager();
  }

  public boolean process(String verzoekFase) {
    LOG.info("Start sending files::" + verzoekFase);
    ServerTransform transform = new ServerTransform("projects-opera-opera-envelope");
    transform.put("oin", context.getOin());
    transform.put("idlevering", context.getIdLevering());
    
    WriteBatcher batcher = manager.newWriteBatcher()
      .withThreadCount(5)
      .withBatchSize(10)
      .withTransform(transform)
      .onBatchSuccess(batch-> {
        System.out.println(
            batch.getTimestamp().getTime() +
            " documents written: " +
            batch.getJobWritesSoFar());
      })
      .onBatchFailure((batch,throwable) -> {
        throwable.printStackTrace();
      });

    DocumentMetadataHandle metadata = new DocumentMetadataHandle();
    metadata.getCollections().addAll(
      String.format("/opera/job/%s", UUID.randomUUID()),
      String.format(OPERA_COLLECTION, verzoekFase.toLowerCase()),
      OPERA_OPTIONS_OPDRACHT_BESTANDEN,
      String.format("/opera/oin/%s", context.getOin()),
      String.format("/opera/idlevering/%s", context.getIdLevering())
    );
    try (ZipFile zip = new ZipFile(context.getZipPayload().toFile())) {
      Enumeration<ZipArchiveEntry> e = zip.getEntries();
      while(e.hasMoreElements()) {
        ZipArchiveEntry entry = e.nextElement();

        if (entry.isDirectory()) {
          continue;
        }
        String uri = String.format("/opera/import-%s/%s/%s/%s", verzoekFase.toLowerCase(), context.getOin(), context.getIdLevering(), entry.getName());
        batcher.addAs(uri, metadata, zip.getInputStream(entry));
      }
      JobTicket ticket = manager.startJob(batcher);
      batcher.flushAndWait();
      manager.stopJob(ticket);
      batcher.awaitCompletion();
      JobReport report = manager.getJobReport(ticket);
      if (report.getFailureEventsCount() > 0) {
        LOG.info(String.format("BATCH ISSUES bij %s::WriteBatcher failed to write %s event(s)", verzoekFase, report.getFailureEventsCount()));
        return false;
      }
      LOG.info("Stop sending files::" + verzoekFase);
      return true;        
    } catch (IOException e) {
      LOG.info(String.format("ZIP IO ISSUES bij %s::", verzoekFase), e);
      return false;
    }
  }
}
