package nl.koop.overheid.lvbb.regisseurmock.marklogic;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;

public class MarkLogicConnection {

  public DatabaseClient getDatabaseClient(MarkLogicProperties mlProps) {
    DatabaseClientFactory.SecurityContext auth = mlProps.getAuthenticationType().equalsIgnoreCase("basic")
        ? new DatabaseClientFactory.BasicAuthContext(mlProps.getWriterUser(), mlProps.getWriterPassword())
        : new DatabaseClientFactory.DigestAuthContext(mlProps.getWriterUser(), mlProps.getWriterPassword());
        
    DatabaseClient client = DatabaseClientFactory.newClient(mlProps.getHost(), mlProps.getPort(), auth);
    return client;
  }
}
