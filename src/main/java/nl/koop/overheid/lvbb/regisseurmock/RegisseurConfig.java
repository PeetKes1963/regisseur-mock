package nl.koop.overheid.lvbb.regisseurmock;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import nl.koop.overheid.lvbb.regisseurmock.marklogic.MarkLogicProperties;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
public class RegisseurConfig {
  private String name;
  private String environment;
  private boolean enabled;

  // make sure the variable has the name of the prefix!!
  private MarkLogicProperties marklogic = new MarkLogicProperties();

  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public String getEnvironment() {
    return environment;
  }
  public void setEnvironment(String environment) {
    this.environment = environment;
  }
  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
      this.enabled = enabled;
  }
  public MarkLogicProperties getMarklogic() {
    return marklogic;
  }
  public void setMarklogic(MarkLogicProperties marklogic) {
    this.marklogic = marklogic;
  }
}
